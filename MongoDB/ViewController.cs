﻿using System;

using AppKit;
using Foundation;

namespace MongoDB
{
    public partial class ViewController : NSViewController
    {
        public ViewController(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            // Do any additional setup after loading the view.

            this.lblActionSelected.StringValue = "Howdy";
        }

        public override NSObject RepresentedObject
        {
            get
            {
                return base.RepresentedObject;
            }
            set
            {
                base.RepresentedObject = value;
                // Update the view, if already loaded.
            }
        }


        partial void btnAction(Foundation.NSObject sender)
        {
            var _value = this.txtValue.StringValue;
            if (!string.IsNullOrEmpty(_value))
            {
                this.lblActionSelected.StringValue = _value;
            }
            else
            {
                this.lblActionSelected.StringValue = "Button Clicked!";
            }
        }
    }
}
