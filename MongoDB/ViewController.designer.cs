// WARNING
//
// This file has been generated automatically by Visual Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace MongoDB
{
	[Register ("ViewController")]
	partial class ViewController
	{
		[Outlet]
		AppKit.NSTextField lblActionSelected { get; set; }

		[Outlet]
		AppKit.NSTextField txtValue { get; set; }

		[Action ("btnAction:")]
		partial void btnAction (Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (lblActionSelected != null) {
				lblActionSelected.Dispose ();
				lblActionSelected = null;
			}

			if (txtValue != null) {
				txtValue.Dispose ();
				txtValue = null;
			}
		}
	}
}
